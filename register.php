<?php
//si le formulaire a ete rempli
 if(isset($_POST['register'])) {
     //si tous les champs ont ete saisi
     if (!empty($_POST['name']) && !empty($_POST['prenom'])
         && !empty($_POST['pseudo'])
         && !empty($_POST['pswd'])
         && !empty($_POST['pswd1'])
         && !empty($_POST['email']))
     {

         $errors = [];
         extract($_POST);


         if (mb_strlen($pseudo) < 3)
             $errors[] = "pseudo trop cour minimum trois caracteres";
     }

     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
         $errors[] = "Adresse email n'est pas valide";
     }


     if (mb_strlen($pswd) < 6) {
         $errors[] = "Votre mot de passe doit avoir au minimum(6 caractères)";
     } 
     else {

         if ($pswd != $pswd1) {
             $errors[] = "les deux mots de passe ne concordent pas";

         }

         if (is_ready_in_use('pseudo', $pseudo, 'users')) {

         }

     }
 else{
         $errors[] = "VEUILLEZ SVP REMPLIR TOUS LES CHAMPS";
     }
 }


?>
<?php require ("views/register.view.php");?>
